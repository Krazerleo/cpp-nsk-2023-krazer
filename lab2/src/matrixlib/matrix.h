#pragma once

#include <algorithm>
#include <iostream>
#include <type_traits>
#include <vector>

template <typename T, int NRows, int NCols> class Matrix {
  static_assert(std::is_same_v<T, float> || std::is_same_v<T, double> ||
                    (std::is_integral_v<T> && !std::is_unsigned_v<T>),
                "only float, double and signed integer types are supported");
  static_assert(NRows > 0, "only positive rows count values are supported");
  static_assert(NCols > 0, "only positive cols count values are supported");

public:
  Matrix() : Matrix(T()) {}
  Matrix(Matrix<T, NRows, NCols> const &other_matrix) = default;
  Matrix &operator=(Matrix<T, NRows, NCols> const &other_matrix) = default;
  ~Matrix() = default;

  explicit Matrix(T const &value) : inner_matrix_(NRows * NCols, value) {}

  static Matrix<T, NRows, NCols> zero() { return Matrix<T, NRows, NCols>(0); }

  static Matrix<T, NRows, NCols> diagonal(T const &value) {
    Matrix<T, NRows, NCols> matrix = zero();

    for (int i = 0; i < std::min(NRows, NCols); i++) {
      matrix.at(i, i) = value;
    }

    return matrix;
  }

  static Matrix<T, NRows, NCols> identity() {
    return Matrix<T, NRows, NCols>::diagonal(1);
  }

  int num_rows() const { return NRows; }

  int num_cols() const { return NCols; }

  int num_values() const { return NCols * NRows; }

  T &at(int row, int col) { return inner_matrix_.at(row * NCols + col); }

  T const &at(int row, int col) const {
    return inner_matrix_.at(row * NCols + col);
  }

  bool operator==(Matrix<T, NRows, NCols> const &other_matrix) const {
    return std::equal(cbegin(), cend(), other_matrix.cbegin());
  }

  bool operator!=(Matrix<T, NRows, NCols> const &other_matrix) const {
    return !(*this == other_matrix);
  }

  Matrix<T, NRows, NCols>
  operator+(Matrix<T, NRows, NCols> const &other_matrix) const {
    Matrix<T, NRows, NCols> result_matrix = zero();
    std::transform(cbegin(), cend(), other_matrix.cbegin(),
                   result_matrix.begin(), std::plus<T>());

    return result_matrix;
  }

  Matrix<T, NRows, NCols> &
  operator+=(Matrix<T, NRows, NCols> const &other_matrix) {
    std::transform(begin(), end(), other_matrix.cbegin(), begin(),
                   std::plus<T>());

    return *this;
  }

  Matrix<T, NRows, NCols> operator+(T scalar) const {
    Matrix<T, NRows, NCols> result_matrix = zero();
    std::transform(cbegin(), cend(), result_matrix.begin(),
                   [&scalar](T const &value) { return value + scalar; });

    return result_matrix;
  }

  Matrix<T, NRows, NCols> &operator+=(T scalar) {
    std::transform(begin(), end(), begin(),
                   [&scalar](T const &value) { return value + scalar; });

    return *this;
  }

  Matrix<T, NRows, NCols>
  operator-(Matrix<T, NRows, NCols> const &other_matrix) const {
    Matrix<T, NRows, NCols> result_matrix = zero();
    std::transform(cbegin(), cend(), other_matrix.cbegin(),
                   result_matrix.begin(), std::minus<T>());

    return result_matrix;
  }

  Matrix<T, NRows, NCols> &
  operator-=(Matrix<T, NRows, NCols> const &other_matrix) {
    std::transform(begin(), end(), other_matrix.cbegin(), begin(),
                   std::minus<T>());

    return *this;
  }

  Matrix<T, NRows, NCols> operator-(T scalar) const {
    Matrix<T, NRows, NCols> result_matrix = zero();
    std::transform(cbegin(), cend(), result_matrix.begin(),
                   [&scalar](T const &value) { return value - scalar; });

    return result_matrix;
  }

  Matrix<T, NRows, NCols> &operator-=(T scalar) {
    std::transform(begin(), end(), begin(),
                   [&scalar](T const &value) { return value - scalar; });

    return *this;
  }

  template <int NColsOther>
  Matrix<T, NRows, NColsOther>
  operator*(Matrix<T, NCols, NColsOther> const &other_matrix) {
    Matrix<T, NRows, NColsOther> result_matrix =
        Matrix<T, NRows, NColsOther>::zero();

    for (int i = 0; i < NRows; i++)
      for (int j = 0; j < NColsOther; j++)
        for (int k = 0; k < NCols; k++)
          result_matrix.at(i, j) += at(i, k) * other_matrix.at(k, j);

    return result_matrix;
  }

  template <int NColsOther>
  Matrix<T, NRows, NColsOther> &
  operator*=(Matrix<T, NCols, NColsOther> const &other_matrix) {
    return *this = (*this) * other_matrix;
  }

  Matrix<T, NRows, NCols> operator*(T scalar) const {
    Matrix<T, NRows, NCols> result_matrix = zero();
    std::transform(cbegin(), cend(), result_matrix.begin(),
                   [&scalar](T const &value) { return value * scalar; });

    return result_matrix;
  }

  Matrix<T, NRows, NCols> &operator*=(T scalar) {
    std::transform(begin(), end(), begin(),
                   [&scalar](T const &value) { return value * scalar; });

    return *this;
  }

  Matrix<T, NRows, NCols> operator/(T scalar) const {
    Matrix<T, NRows, NCols> result_matrix = zero();
    std::transform(cbegin(), cend(), result_matrix.begin(),
                   [&scalar](T const &value) { return value / scalar; });

    return result_matrix;
  }

  Matrix<T, NRows, NCols> &operator/=(T scalar) {
    std::transform(begin(), end(), begin(),
                   [&scalar](T const &value) { return value / scalar; });

    return *this;
  }

  typename std::vector<T>::iterator begin() { return inner_matrix_.begin(); }

  typename std::vector<T>::iterator end() { return inner_matrix_.end(); }

  typename std::vector<T>::const_iterator cbegin() const {
    return inner_matrix_.cbegin();
  }

  typename std::vector<T>::const_iterator cend() const {
    return inner_matrix_.cend();
  }

  typename std::vector<T>::reverse_iterator rbegin() {
    return inner_matrix_.rbegin();
  }

  typename std::vector<T>::reverse_iterator rend() {
    return inner_matrix_.rend();
  }

  typename std::vector<T>::const_reverse_iterator crbegin() const {
    return inner_matrix_.crbegin();
  }

  typename std::vector<T>::const_reverse_iterator crend() const {
    return inner_matrix_.crend();
  }

private:
  std::vector<T> inner_matrix_;
};

template <typename T, int NRows, int NCols>
std::ostream &operator<<(std::ostream &os,
                         Matrix<T, NRows, NCols> const &out_matrix) {
  for (int i = 0; i < NRows; i++) {
    os << '[';
    for (int j = 0; j < NCols - 1; j++) {
      os << out_matrix.at(i, j) << ' ';
    }
    os << out_matrix.at(i, NCols - 1) << "]\n";
  }

  return os;
}

template <typename T, int NRows, int NCols>
double det(Matrix<T, NRows, NCols> const &matrix) {
  if (matrix.num_rows() != matrix.num_cols()) {
    throw std::runtime_error(
        "Determinant of rectange matrix is not implemented");
  }

  if (matrix.num_rows() > 3) {
    throw std::runtime_error(
        "Determinant of matrix with size greater than 3 is not implemented");
  }

  if (matrix.num_rows() == 1) {
    return matrix.at(0, 0);
  }
  if (matrix.num_rows() == 2) {
    return matrix.at(0, 0) * matrix.at(1, 1) -
           matrix.at(0, 1) * matrix.at(1, 0);
  }
  if (matrix.num_rows() == 3) {
    return (matrix.at(0, 0) * matrix.at(1, 1) * matrix.at(2, 2) +
            matrix.at(0, 1) * matrix.at(1, 2) * matrix.at(2, 0) +
            matrix.at(0, 2) * matrix.at(1, 0) * matrix.at(2, 1)) -
           (matrix.at(0, 2) * matrix.at(1, 1) * matrix.at(2, 0) +
            matrix.at(0, 0) * matrix.at(1, 2) * matrix.at(2, 1) +
            matrix.at(2, 2) * matrix.at(1, 0) * matrix.at(0, 1));
  }

  throw std::runtime_error("Undefined error");
}
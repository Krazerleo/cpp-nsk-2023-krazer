#include <iostream>

#include "matrixlib/matrix.h"

int main(int, char **) {
  auto mat1 = Matrix<int, 2, 2>::diagonal(1);
  auto mat2 = Matrix<int, 2, 2>::diagonal(5);

  std::cout << mat1 * mat2;
}

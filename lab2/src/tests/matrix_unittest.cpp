#include "matrixlib/matrix.h"
#include "gtest/gtest.h"
#include <array>

TEST(Matrix, zero_matrix_ctor_uniform) {
  auto mat = Matrix<int, 2, 2>::zero();

  std::array<int, 4> check_array = {0, 0, 0, 0};
  EXPECT_TRUE(std::equal(mat.cbegin(), mat.cend(), check_array.cbegin()));
}

TEST(Matrix, zero_matrix_ctor_nonuniform) {
  auto mat = Matrix<int, 3, 2>::zero();

  std::array<int, 6> check_array = {0, 0, 0, 0, 0, 0};
  EXPECT_TRUE(std::equal(mat.cbegin(), mat.cend(), check_array.cbegin()));
}

TEST(Matrix, diagonal_matrix_ctor_uniform) {
  auto mat = Matrix<int, 2, 2>::diagonal(2);

  std::array<int, 4> check_array = {2, 0, 0, 2};
  EXPECT_TRUE(std::equal(mat.cbegin(), mat.cend(), check_array.cbegin()));
}

TEST(Matrix, diagonal_matrix_ctor_nonuniform) {
  auto mat = Matrix<int, 2, 3>::diagonal(2);

  std::array<int, 6> check_array = {2, 0, 0, 0, 2, 0};
  EXPECT_TRUE(std::equal(mat.cbegin(), mat.cend(), check_array.cbegin()));
}

TEST(Matrix, deleted_move_ctor) {
  auto mat1 = Matrix<int, 2, 2>::zero();
  mat1.at(0, 0) = 5;
  mat1.at(0, 1) = 16;
  mat1.at(1, 0) = 20;
  mat1.at(1, 1) = 123;

  auto mat2 = std::move(mat1);

  ASSERT_TRUE(std::distance(mat1.begin(), mat1.end()) == 4);
  ASSERT_TRUE(std::distance(mat2.begin(), mat2.end()) == 4);
  EXPECT_TRUE(mat1.at(1, 1) == mat2.at(1, 1));
}

TEST(Matrix, reverse_iterator) {
  auto mat = Matrix<int, 2, 2>::zero();
  mat.at(0, 0) = 5;
  mat.at(0, 1) = 16;
  mat.at(1, 0) = 20;
  mat.at(1, 1) = 123;

  auto expected_result = std::vector<int>{123, 20, 16, 5};

  EXPECT_TRUE(std::equal(mat.crbegin(), mat.crend(), expected_result.begin()));
}

TEST(Matrix, plus_operator_nonuniform) {
  auto mat1 = Matrix<int, 2, 3>::zero();
  mat1.at(0, 0) = 5;
  mat1.at(1, 1) = 10;
  mat1.at(1, 2) = 20;

  auto mat2 = Matrix<int, 2, 3>::zero();
  mat2.at(0, 0) = 12;
  mat2.at(0, 2) = 23;
  mat2.at(1, 0) = 14;

  auto check_mat1 = mat1 + mat2;
  auto check_mat2 = mat2 + mat1;

  std::array<int, 6> check_array = {17, 0, 23, 14, 10, 20};
  EXPECT_TRUE(
      std::equal(check_mat1.cbegin(), check_mat1.cend(), check_array.cbegin()));
  EXPECT_TRUE(
      std::equal(check_mat2.cbegin(), check_mat2.cend(), check_array.cbegin()));
}

TEST(Matrix, plus_operator_matrix) {
  auto mat1 = Matrix<int, 2, 2>::zero();
  mat1.at(0, 0) = 5;
  mat1.at(1, 1) = 10;

  auto mat2 = Matrix<int, 2, 2>::zero();
  mat2.at(0, 0) = 12;
  mat2.at(1, 0) = 14;

  auto check_mat1 = mat1 + mat2;
  auto check_mat2 = mat2 + mat1;

  std::array<int, 4> check_array = {17, 0, 14, 10};
  EXPECT_TRUE(
      std::equal(check_mat1.cbegin(), check_mat1.cend(), check_array.cbegin()));
  EXPECT_TRUE(
      std::equal(check_mat2.cbegin(), check_mat2.cend(), check_array.cbegin()));
}

TEST(Matrix, plus_operator_scalar) {
  auto mat = Matrix<int, 2, 2>::zero();
  mat.at(0, 0) = 5;
  mat.at(1, 1) = 10;

  auto check_mat = mat + 2;

  std::array<int, 4> check_array = {7, 2, 2, 12};
  EXPECT_TRUE(
      std::equal(check_mat.cbegin(), check_mat.cend(), check_array.cbegin()));
}

TEST(Matrix, minus_operator_matrix) {
  auto mat1 = Matrix<int, 2, 2>::zero();
  mat1.at(0, 0) = 5;
  mat1.at(0, 1) = 16;
  mat1.at(1, 0) = 20;
  mat1.at(1, 1) = 6;

  auto mat2 = Matrix<int, 2, 2>::zero();
  mat2.at(0, 0) = 7;
  mat2.at(0, 1) = 14;
  mat2.at(1, 0) = 23;
  mat2.at(1, 1) = 8;

  auto check_mat1 = mat1 - mat2;
  auto check_mat2 = mat2 - mat1;

  std::array<int, 4> check_array1 = {-2, 2, -3, -2};
  std::array<int, 4> check_array2 = {2, -2, 3, 2};
  EXPECT_TRUE(std::equal(check_mat1.cbegin(), check_mat1.cend(),
                         check_array1.cbegin()));
  EXPECT_TRUE(std::equal(check_mat2.cbegin(), check_mat2.cend(),
                         check_array2.cbegin()));
}

TEST(Matrix, minus_operator_scalar) {
  auto mat = Matrix<int, 2, 2>::zero();
  mat.at(0, 0) = 5;
  mat.at(1, 1) = 10;

  auto check_mat = mat - 2;

  std::array<int, 4> check_array = {3, -2, -2, 8};

  EXPECT_TRUE(
      std::equal(check_mat.cbegin(), check_mat.cend(), check_array.cbegin()));
}

TEST(Matrix, multitply_operator_matrix_uniform) {
  auto mat1 = Matrix<int, 2, 2>::zero();
  mat1.at(0, 0) = 5;
  mat1.at(0, 1) = 16;
  mat1.at(1, 0) = 20;
  mat1.at(1, 1) = 6;

  auto mat2 = Matrix<int, 2, 2>::zero();
  mat2.at(0, 0) = 7;
  mat2.at(0, 1) = 14;
  mat2.at(1, 0) = 23;
  mat2.at(1, 1) = 8;

  auto check_mat1 = mat1 * mat2;
  auto check_mat2 = mat2 * mat1;

  std::array<int, 4> check_array1 = {403, 198, 278, 328};
  std::array<int, 4> check_array2 = {315, 196, 275, 416};
  EXPECT_TRUE(std::equal(check_mat1.cbegin(), check_mat1.cend(),
                         check_array1.cbegin()));
  EXPECT_TRUE(std::equal(check_mat2.cbegin(), check_mat2.cend(),
                         check_array2.cbegin()));
}

TEST(Matrix, multitply_operator_matrix_nonuniform) {
  auto mat1 = Matrix<int, 2, 3>::zero();
  mat1.at(0, 0) = 5;
  mat1.at(0, 1) = 16;
  mat1.at(1, 0) = 20;
  mat1.at(1, 1) = 6;

  auto mat2 = Matrix<int, 3, 2>::zero();
  mat2.at(0, 0) = 7;
  mat2.at(0, 1) = 14;
  mat2.at(1, 0) = 23;
  mat2.at(1, 1) = 8;

  auto check_mat1 = mat1 * mat2;
  auto check_mat2 = mat2 * mat1;

  std::array<int, 4> check_array1 = {403, 198, 278, 328};
  std::array<int, 9> check_array2 = {315, 196, 0, 275, 416, 0, 0, 0, 0};
  EXPECT_TRUE(std::equal(check_mat1.cbegin(), check_mat1.cend(),
                         check_array1.cbegin()));
  EXPECT_TRUE(std::equal(check_mat2.cbegin(), check_mat2.cend(),
                         check_array2.cbegin()));
}

TEST(Matrix, multitply_operator_scalar) {
  auto mat = Matrix<int, 2, 2>::zero();
  mat.at(0, 0) = 5;
  mat.at(1, 1) = 10;

  auto check_mat = mat * 2;

  std::array<int, 4> check_array = {10, 0, 0, 20};

  EXPECT_TRUE(
      std::equal(check_mat.cbegin(), check_mat.cend(), check_array.cbegin()));
}

TEST(Matrix, divide_operator_scalar) {
  auto mat = Matrix<int, 2, 2>::zero();
  mat.at(0, 0) = 5;
  mat.at(1, 1) = 10;

  auto check_mat = mat / 5;

  std::array<int, 4> check_array = {1, 0, 0, 2};

  EXPECT_TRUE(
      std::equal(check_mat.cbegin(), check_mat.cend(), check_array.cbegin()));
}

TEST(Matrix, find_determinant_3x3) {
  auto mat = Matrix<int, 3, 3>::zero();
  mat.at(0, 0) = 5;
  mat.at(0, 1) = 16;
  mat.at(0, 2) = 20;
  mat.at(1, 0) = 123;
  mat.at(1, 1) = 11;
  mat.at(1, 2) = 43;
  mat.at(2, 0) = 2;
  mat.at(2, 1) = -1;
  mat.at(2, 2) = -23;

  double result = det(mat);
  EXPECT_TRUE(result == 42690);
}

TEST(Matrix, find_determinant_2x2) {
  auto mat = Matrix<int, 2, 2>::zero();
  mat.at(0, 0) = 5;
  mat.at(0, 1) = 16;
  mat.at(1, 0) = 20;
  mat.at(1, 1) = 123;

  double result = det(mat);
  EXPECT_TRUE(result == 295);
}

TEST(Matrix, find_determinant_1x1) {
  auto mat = Matrix<int, 1, 1>::zero();
  mat.at(0, 0) = 5;

  double result = det(mat);
  EXPECT_TRUE(result == 5);
}

TEST(Matrix, dump_to_stdout) {
  auto mat = Matrix<int, 3, 3>::zero();
  mat.at(0, 0) = 5;
  mat.at(0, 1) = 16;
  mat.at(0, 2) = 9;
  mat.at(1, 0) = 20;
  mat.at(1, 1) = 123;
  mat.at(1, 2) = 125;
  mat.at(2, 0) = 21;
  mat.at(2, 1) = 124;
  mat.at(2, 2) = 130;

  std::string expect_string = "[5 16 9]\n"
                              "[20 123 125]\n"
                              "[21 124 130]\n";

  std::stringstream str_stream;
  str_stream << mat;
  EXPECT_TRUE(str_stream.str() == expect_string);
}